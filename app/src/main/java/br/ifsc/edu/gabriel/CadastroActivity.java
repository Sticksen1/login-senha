package br.ifsc.edu.gabriel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CadastroActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    EditText loginCadastro,senhaCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        firebaseAuth = FirebaseAuth.getInstance();

        loginCadastro = findViewById(R.id.editTextEmailCadastro);
        senhaCadastro = findViewById(R.id.editTextSenhaCadastro);

    }

    public void cadastrarNovoUsuario(View view) {

        if(!loginCadastro.getText().toString().trim().equals("")) {

            firebaseAuth.createUserWithEmailAndPassword(loginCadastro.getText().toString(), senhaCadastro.getText().toString())
            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getApplicationContext(), "Cadastro efetuado!", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext()
                                , "Falha no cadastro!" , Toast.LENGTH_LONG).show();
                    }
                }
            });


        }

    }
}
