package br.ifsc.edu.gabriel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    EditText editTextLogin,editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        editTextLogin = findViewById(R.id.editText4);
        editTextSenha = findViewById(R.id.editTextSenha);

        mAuth = FirebaseAuth.getInstance();

        //Cria nova conta
//        mAuth.createUserWithEmailAndPassword("gabriel@ifsc.edu.br", "123muda");
//        mAuth.signInWithEmailAndPassword("gabriel@ifsc.edu.br", "123muda")
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if(task.isSuccessful()){
//                    Toast.makeText(getApplicationContext(),mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
//                    Intent i = new Intent(getApplicationContext(), PrincipalActivity.class);
//                    startActivity(i);
//
//                }else{
//                    Toast.makeText(getApplicationContext(), "Falha no login", Toast.LENGTH_LONG).show();
//                }
//
//            }
//        });
//        FirebaseUser firebaseUser = mAuth.getCurrentUser();
//
//        if(firebaseUser != null){
//            Log.d("FirebaseUserExemplo", "Usuário Logado" + firebaseUser.getEmail());
//
//
//        }else{
//            Log.d("FirebaseUserExemplo", "Falha na autenticação");
//
//        }
    }

    public void login(View view) {

        mAuth.signOut();

        final String login = editTextLogin.getText().toString();
        String senha = editTextSenha.getText().toString();

        if(!login.trim().equals("")){

            mAuth.signInWithEmailAndPassword(login,senha)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), "Login efetuado", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getApplicationContext()
                                        , "Login não efetuado " + login + "!" , Toast.LENGTH_LONG).show();
                            }
                        }
                    });

            FirebaseUser firebaseUser = mAuth.getCurrentUser();

            if(firebaseUser != null){

                Intent intent = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(intent);
            }
        }

    }

    public void cadastro(View view) {

        Intent icadastro = new Intent(getApplicationContext(), CadastroActivity.class);
        startActivity(icadastro);

    }

    public void recuperaSenha(View view) {

        if(!editTextLogin.getText().toString().trim().equals("")) {
            mAuth.sendPasswordResetEmail(editTextLogin.getText().toString());
        }
    }
}
